/**
 * 使用云函数获取openid，登陆
 */
const db = wx.cloud.database()

export function login (params) {
  return wx.cloud.callFunction({
    name: 'login'
  })
};

// 获取用户信息
export function getUser (openId) {
  return db.collection('user').where({_openid: openId}).get()
};

// 创建用户
export function createUser (data) {
  return db.collection('user').add({ data })
};

// 创建用户
export function updateUser (id,data) {
  return db.collection('user').doc(id).set({ data })
};

export default {
  getUser,
  createUser,
  updateUser,
  login
}
