import { add } from './cloud.js'
const db = wx.cloud.database()
const _ = db.command

// 获取全部文章
export function getAllRelease () {
  return db.collection('release').orderBy('createTime', 'desc').get()
}

export default {
  getAllRelease
}
