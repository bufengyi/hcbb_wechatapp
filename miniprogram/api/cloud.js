// 封装方法
wx.cloud.init()
export function add (params) {
  return wx.cloud.callFunction({
    name: 'add',
    data: {
      dbName: params.name,
      data: params.data
    }
  })
}

export function update (params) {
  return wx.cloud.callFunction({
    name: 'update',
    data: {
      dbName: params.name,
      id: params.id,
      data: params.data
    }
  })
}

export default {
  update,
  add
}
