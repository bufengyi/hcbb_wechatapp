/**
 * 调用内容安全云函数
 */
// 核验内容是否安全
export function ContentCheck (msg) {
  return wx.cloud.callFunction({
    name: 'ContentCheck',
    data: {
      msg
    }
  })
};

// 图片核验是否违规
export function imageCheck (img) {
  return wx.cloud.callFunction({
    name: 'ContentCheck',
    data: {
      img
    }
  })
};

export default {
  imageCheck,
  ContentCheck
}
