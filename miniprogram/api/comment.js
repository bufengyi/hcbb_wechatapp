import { update } from '../utils/cloud.js'
const db = wx.cloud.database()
// 获取文章内容
export function getarticle (data) {
  return db.collection('release').doc(data).get()
}
// 更新访问量
export function updatebrowse (id, data) {
  return update({
    name: 'release',
    id,
    data
  })
}
// 创建评论
export function addcomment (data) {
  return db.collection('comment').add({data})
}
// 获取评论
export function getcomment (data) {
  return db.collection('comment').orderBy('createTime', 'desc').where(data).get()
}
// 获取评论数量
export function getcommentcount (data) {
  return db.collection('comment').where(data).count()
}

export default {
  getarticle,
  getcomment,
  addcomment,
  getcommentcount
}
