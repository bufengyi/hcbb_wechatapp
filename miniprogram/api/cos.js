
import { timeste } from '@/utils/index'
/**
 * 使用云存储存储图片
 * 文件名及目录生成规律： 根目录为upload，二级目录为年year月month日day,文件名为当前时间time
 */
export function uploads (file) {
  const filePath = file
  const data = new Date()
  const time = data.getTime()
  const gettimes = timeste(data)
  const cloudPath = 'upload/' + gettimes + '/' + time + filePath.match(/\.[^.]+?$/)[0]
  return wx.cloud.uploadFile({
    cloudPath,
    filePath
  })
}

export default {
  uploads
}
