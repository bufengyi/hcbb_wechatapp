// pages/home/home.js
const app = getApp();
Page({
  data: {
    titleName: "社区",
    tabBar: "community"
  },

  titleName(e) {
    this.setData({
      titleName: e.detail
    })
  },

  tabBar(e) {
    this.setData({
      tabBar: e.detail
    })
  }
})