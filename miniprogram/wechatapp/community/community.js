const app = getApp();
import { getAllRelease } from '../../api/release'
Component({
    options: {
        addGlobalClass: true
    },
    data: {
        tab: "all",
        list: [
            {
                name: "全部",
                tab: "all"
            },
            {
                name: "热门",
                tab: "host"
            },
            {
                name: "文章",
                tab: "article"
            },
            {
                name: "问答",
                tab: "question"
            }
        ],
        screenHeight: (app.globalData.screenHeight - app.globalData.CustomBar) * app.globalData.pxToRpxScale,
        triggered: false,
        lastY: 0,
        models: []
    },
    attached() {
        this.setData({
            triggered: true
        })
        this.getRelease()
    },
    methods: {
        async getRelease() {
            const res = await getAllRelease()
            this.setData({
                models: res.data
            })
        },
        select(e) {
            this.setData({
                triggered: true,
                tab: e.currentTarget.dataset.tab
            })
        },
        /**
         * 判断滑动方向下拉刷新
         */
        touchStart(e) {
            this.setData({
                lastY: e.touches[0].pageY
            })
        },
        touchMove(event) {
            let currenty = event.touches[0].pageY
            let ty = currenty - this.data.lastY
            if (ty > 0) {
                this.setData({
                    triggered: true,
                    lastY: 0
                })
            }
        },
        /**
         * 触发自定义下拉刷新事件
         */
        onRefresh(e) {
            if (this._freshing) return
            this._freshing = true
            setTimeout(() => {
                this.setData({
                    triggered: false,
                })
                this._freshing = false
            }, 1000)
        },
        /**
         * 下拉刷新终止事件
         */
        onRestore(e) {
        },
        /**
         * 下拉抵达底部
         */
        onTolower(e) {
        }
    }
})
