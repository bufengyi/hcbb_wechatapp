//app.js
App({
  onLaunch: function () {
    // 获取系统信息
    wx.getSystemInfo({
      success: e => {
        // 检测基础库版本
        var version = e.SDKVersion.replace(/\./g, "")
        if(parseInt(version)<2101){
          wx.showModal({
            title: '提示',
            content: '该微信版本暂不支持部分功能，如需正常使用请更新！'
          })
        }
        // px转换到rpx的比例
        this.globalData.pxToRpxScale = 750 / e.windowWidth;
        // 状态栏的高度
        this.globalData.statusBarHeight = e.statusBarHeight
        // window的宽度
        this.globalData.windowWidth = e.windowWidth
        // window的高度
        this.globalData.windowHeight = e.windowHeight
        // 屏幕的高度
        this.globalData.screenHeight = e.screenHeight
        let capsule = wx.getMenuButtonBoundingClientRect();
        if (capsule) {
          this.globalData.Custom = capsule;
          this.globalData.CustomBar = capsule.bottom + capsule.top - e.statusBarHeight;
        } else {
          this.globalData.CustomBar = e.statusBarHeight + 50;
        }
      }
    })
    if (!wx.cloud) {
      console.error('请使用 2.2.3 或以上的基础库以使用云能力')
    } else {
      wx.cloud.init({
        traceUser: true,
      })
    }
  },
  globalData: {
    userInfo: null
  },
  tabBar: {
    list: [
      {
        pagePath: "community",
        iconPath: "cuIcon-communityfill",
        text: "社区"
      },
      {
        pagePath: "repair",
        iconPath: "cuIcon-repairfill",
        text: "报修"
      },
      {
        pagePath: "index",
        iconPath: "cuIcon-newfill",
        text: "工具"
      },
      {
        pagePath: "user",
        iconPath: "cuIcon-myfill",
        text: "我的"
      }
    ]
  }
})
