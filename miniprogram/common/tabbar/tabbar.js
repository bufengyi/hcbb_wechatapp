const app = getApp();
Component({
  /**
   * 组件的一些选项
   */
  options: {
    addGlobalClass: true
  },
  /**
   * 组件的初始数据
   */
  data: {
    tabBar: "community",
    list: app.tabBar.list
  },
  methods: {
    /**
     * 底部导航事件
     */
    selectTabBar: function (e) {
      this.setData({
        tabBar: e.currentTarget.dataset.bar
      })
      var tabBar = this.data.list.filter(item => item.pagePath == e.currentTarget.dataset.bar)
      this.triggerEvent('titleName',tabBar[0].text)
      this.triggerEvent('tabBar',tabBar[0].pagePath)
    }
  }
})