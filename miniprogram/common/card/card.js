const app = getApp();
Component({
  /**
   * 组件的对外属性
   */
  properties: {
    models: {
      type: Array
    }
  },
  /**
   * 组件的一些选项
   */
  options: {
    addGlobalClass: true
  },
  attached() {
  }
})