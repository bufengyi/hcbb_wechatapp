const app = getApp();
Component({
  /**
   * 组件的对外属性
   */
  properties: {
    titleName: {
      type: String,
      default: "惠城帮帮"
    }
  },
  /**
   * 组件的一些选项
   */
  options: {
    addGlobalClass: true
  },
  /**
   * 组件的初始数据
   */
  data: {
    statusBarHeight: app.globalData.statusBarHeight,
    CustomBar: app.globalData.CustomBar
  },
  methods: {
  }
})