import { login, getUser, createUser, updateUser } from '../../api/login'
Component({
  /**
   * 组件的一些选项
   */
  options: {
    addGlobalClass: true
  },
  /**
   * 组件的初始数据
   */
  data: {
    image: '../../images/auth.jpeg',
    isShow: false
  },
  /**
   * created 组件实例化，但节点树还未导入，因此这时不能用setData
   * attached 节点树完成，可以用setData渲染节点，但无法操作节点
   * ready 组件布局完成，这时可以获取节点信息，也可以操作节点
   * moved 组件实例被移动到树的另一个位置
   * detached 组件实例从节点树中移除
   */
  attached() {
    var openid = wx.getStorageSync('openid')
    if (!openid) {
      this.setData({
        isShow: true
      })
    }
  },
  methods: {
    async userLogin(e) {
      if (e.detail.userInfo) {
        await login().then(async res => {
          // 检测更新用户信息
          const user = await getUser(res.result.openid)
          if (!user.data[0]) {
            await createUser(e.detail.userInfo)
          } else {
            await updateUser(user.data[0]._id, e.detail.userInfo)
          }
          wx.setStorageSync('userInfo', e.detail.userInfo);
          wx.setStorageSync('openid', res.result.openid);
          this.setData({
            isShow: false
          })
          wx.showToast({
            title: '已授权',
            icon: 'success',
            duration: 1000
          })
        }).catch(er => {
          console.log(er)
        })
      }
    }
  }
})