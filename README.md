# 惠城帮帮小程序

#### 未获作者允许禁止商用
#### 未获作者允许禁止商用
#### 未获作者允许禁止商用 (重要事情说三遍)

####  :shipit: 介绍

惠城帮帮小程序是一款集校园互助、娱乐、义修服务于一体的类似博客论坛的小程序，在这里你能在帮帮圈提出校园内的问题（电脑问题，学习环境，网络问题等），尽你所能解答师弟师妹的疑惑；同时还可以在这个小程序中的找帮帮进行电脑小问题的报修，由惠州城市职业学院计算机协会的成员来进行义修活动（仅支持惠城院师生，暂未支持校外的人员）。
该小程序的是由一名在校大三实习生独立完成的小作品，合作请加微信：s455171924

#### 开发环境

后端采用[小程序云开发](https://developers.weixin.qq.com/miniprogram/dev/wxcloud/basis/getting-started.html)  
前端UI采用[ColorUi框架](https://www.color-ui.com/)

#### 软件架构

```
├── cloud                   云函数目录
├── miniprogram             小程序目录
|   ├── api                 api接口目录
|   ├── common              公共组件自定义插件目录
|   ├── images              静态资源目录
|   ├── wechatapp           页面目录
|   |   ├── home            主页目录
|   |   └── community       社区页面
|   ├── app.js              全局逻辑
|   ├── app.json            全局配置
|   ├── app.wxss            全局样式
|   └── sitemap.json        sitemap配置文件
├── project.config.json     配置文件
└── README.md               说明文档 
```

#### 使用说明

使用该源码需在[小程序开发工具](https://developers.weixin.qq.com/miniprogram/dev/devtools/download.html)才能运行

#### 参与贡献

主要作者： Mouxan
